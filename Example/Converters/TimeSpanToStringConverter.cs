﻿using Microsoft.Maui.Controls;
using System;
using System.Globalization;

namespace Example
{
    public class TimeSpanToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo cultureInfo)
        {
            TimeSpan time = (TimeSpan)value;
            if (time.TotalHours < 1)
                return time.ToString(@"mm\:ss\.ff");
            return time.ToString(@"hh\:mm\:ss\.ff");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo cultureInfo)
        {
            var source = value as string;
            var res =  TimeSpan.TryParseExact(source, @"hh\:mm\:ss\.ff", null, out var result);
            
            if (res)
                return result;
            return TimeSpan.ParseExact(source, @"mm\:ss\.ff", null);
        }
    }
}