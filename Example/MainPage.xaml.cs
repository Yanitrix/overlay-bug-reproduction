﻿using Microsoft.Maui.Controls;
using ReactiveUI;
using System.Threading.Tasks;

namespace Example;

public partial class MainPage : ContentPage, IViewFor<SessionViewModel>
{
	public MainPage()
	{
		InitializeComponent();
        ViewModel = new SessionViewModel(Dispatcher);
        BindingContext = ViewModel;

        this.WhenActivated(d =>
            {
                d(this
                    .ViewModel
                    .ConfirmCancel
                    .RegisterHandler(
                        async interaction =>
                        {
                            var deleteIt = await this.DisplayCancelSessionDialog();
                            interaction.SetOutput(deleteIt);
                        }));
            });
    }

    public SessionViewModel ViewModel { get; set; }
    object IViewFor.ViewModel { get => ViewModel; set => ViewModel = (SessionViewModel)value; }

    private async Task<bool> DisplayCancelSessionDialog()
    {
        var result = await DisplayAlert("Cancel session", "Are you sure you want to cancel session?", "Yes", "No");
        return result;
    }
}