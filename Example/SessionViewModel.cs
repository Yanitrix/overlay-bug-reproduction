﻿using Microsoft.Maui.Dispatching;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Input;
using ITimer = System.Timers.Timer;
using Timer = System.Timers.Timer;

namespace Example
{
    public class SessionViewModel : ReactiveObject
    {
        private readonly ITimer timer;

        public ICommand StartSession { get; }

        public Interaction<string, bool> ConfirmCancel { get; }

        public ICommand CancelSession { get; }

        public ICommand Start { get; }

        public ICommand Stop { get; }

        [Reactive]
        public bool SessionStarted { get; set; }

        [Reactive]
        public bool TimerRunning { get; set; }

        [Reactive]
        public TimeSpan Time { get; set; }

        private readonly IDispatcher dispatcher;

        public SessionViewModel(IDispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
            timer = new Timer();
            timer.Elapsed += Elapsed;

            Start = ReactiveCommand.Create(
                StartTimer,
                this.WhenAnyValue(
                    vm => vm.TimerRunning,
                    vm => vm.SessionStarted,
                    (timerRunning, sessionStarted) => !timerRunning && sessionStarted)
            );

            Stop = ReactiveCommand.Create(StopTimer, this.WhenAnyValue(vm => vm.TimerRunning));
            StartSession = ReactiveCommand.Create(() => SessionStarted = true, this.WhenAnyValue(vm => vm.SessionStarted).Select(x => !x));
            ConfirmCancel = new();
            CancelSession = ReactiveCommand.CreateFromTask(Purge, this.WhenAnyValue(vm => vm.SessionStarted));

        }

        private async Task Purge()
        {
            if (Time == default)
            {
                SessionStarted = false;
                return;
            }

            StopTimer();
            var cancel = await ConfirmCancel.Handle(default!);

            if (cancel)
            {
                SessionStarted = false;
                Time = default;
            }
        }


        private void StartTimer()
        {
            timer.Start();
            TimerRunning = true;
        }

        private void StopTimer()
        {
            timer.Stop();
            TimerRunning = false;
        }

        private async void Elapsed(object? sender, ElapsedEventArgs e)
        {
            await dispatcher.DispatchAsync(() =>
            {
                Time = Time.Add(TimeSpan.FromMilliseconds(100));
            });
        }
    }
}